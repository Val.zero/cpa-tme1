mod edge_list;
mod adjacency_matrix;
mod adjacency_array;

use std::path::Path;
use std::fs::File;
use std::io::{BufReader, BufRead};
use std::time::Instant;

fn parse_input(path: &str) -> (usize, usize) {
    let input_path = Path::new(path);
    let f = File::open(input_path).unwrap();
    let reader = BufReader::new(f);
    let lines = reader.lines().map(|l| l.unwrap());
    let mut nodes_list = vec![false];
    let mut edges = 0;
    for line in lines {
        // println!("reading : {}", line);
        if line.chars().next().unwrap() != '#' {
            edges += 1;
            let nodes = line.split('\t').map(|n| n.parse::<usize>().unwrap()).collect::<Vec<usize>>();
            for node in nodes {
                if node > nodes_list.len() - 1 {
                    nodes_list.resize(node + 1, false);
                }
                nodes_list[node] = true;
            }
        }
    }
    let mut nodes = 0;
    for val in nodes_list {
        if val {
            nodes += 1;
        }
    }
    (nodes, edges)
}

fn main() {
    //let path = "./assets/com-amazon.ungraph.txt";
    let path = "./assets/com-lj.ungraph.txt";
    //let path = "./assets/com-orkut.ungraph.txt";
    //let path = "./assets/com-amazon.ungraph.txt";
    let (nodes, edges) = parse_input(path);
    println!("nodes : {}; edges : {}", nodes, edges);
    // let edge_list = edge_list::make_edge_list(path);
    // println!("edge list size : {}", edge_list.len());
    // let adjacency_matrix = adjacency_matrix::make_adjacency_matrix(path);
    // println!("edeges in adj mat : {}", adjacency_matrix::count_edges(&adjacency_matrix));
    let t1 = Instant::now();
    let adjacency_array = adjacency_array::make_adjacency_array(path, nodes);
    let t2 = Instant::now();
    println!("edges in adj array : {}", adjacency_array::count_edges(&adjacency_array));
    //let (n1, half_upper_bound) = adjacency_array::bfs(&adjacency_array, 1);
    let t3 = Instant::now();
    //let (n2, lower_bound) = adjacency_array::bfs(&adjacency_array, n1);
    let t4 = Instant::now();
    let triangles = adjacency_array::triangles(&adjacency_array);
    let t5 = Instant::now();
    let making_time = t2.duration_since(t1).as_millis();
    let bfs1_time = t3.duration_since(t2).as_millis();
    let bfs2_time = t4.duration_since(t3).as_millis();
    let calc_time = t4.duration_since(t2).as_millis();
    let tri_time = t5.duration_since(t4).as_millis();
    //println!("Estimated lower bound for diameter : {} (between {} and {})", lower_bound, n1, n2);
    // TODO: better method for upper bound ?
    //println!("Estimated upper bound for diameter : {} (2 times distance between {} and {}", half_upper_bound, 1, n1);
    println!("Number of triangles : {}", triangles);
    println!("Charging time : {}ms", making_time);
    //println!("Calculation time : {}ms (1st BFS : {}ms; 2nd BFS : {}ms)", calc_time, bfs1_time, bfs2_time);
    println!("Time to find triangles : {}ms", tri_time);
}
