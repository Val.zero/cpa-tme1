use std::path::Path;
use std::fs::File;
use std::io::{BufReader, BufRead};

pub fn make_edge_list(path: &str) -> Vec<(usize, usize)> {
    let input_path = Path::new(path);
    let f = File::open(input_path).unwrap();
    let reader = BufReader::new(f);
    let lines = reader.lines().map(|l| l.unwrap());
    let mut res = vec![];
    for line in lines {
        // println!("reading : {}", line);
        if line.chars().next().unwrap() != '#' {
            let nodes = line.split('\t').map(|n| n.parse::<usize>().unwrap()).collect::<Vec<usize>>();
            let n1 = nodes.get(0).unwrap();
            let n2 = nodes.get(1).unwrap();
            res.push((*n1, *n2));
        }
    }
    res
}