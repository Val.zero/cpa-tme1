use std::path::Path;
use std::fs::File;
use std::io::{BufReader, BufRead};

fn get_max_node(path: &str) -> usize {
    let input_path = Path::new(path);
    let f = File::open(input_path).unwrap();
    let reader = BufReader::new(f);
    let lines = reader.lines().map(|l| l.unwrap());
    let mut max = 0;
    for line in lines {
        // println!("reading : {}", line);
        if line.chars().next().unwrap() != '#' {
            let nodes = line.split('\t').map(|n| n.parse::<usize>().unwrap()).collect::<Vec<usize>>();
            for node in nodes {
                if node > max {
                    max = node;
                }
            }
        }
    }
    max
}

pub fn make_adjacency_matrix(path: &str) -> Vec<Vec<bool>> {
    let max_node = get_max_node(path);
    let mut res = Vec::with_capacity(max_node);
    for _ in 0..max_node {
        let col = vec![false; max_node];
        res.push(col);
    }

    let input_path = Path::new(path);
    let f = File::open(input_path).unwrap();
    let reader = BufReader::new(f);
    let lines = reader.lines().map(|l| l.unwrap());
    for line in lines {
        // println!("reading : {}", line);
        if line.chars().next().unwrap() != '#' {
            let nodes = line.split('\t').map(|n| n.parse::<usize>().unwrap()).collect::<Vec<usize>>();
            let x = *nodes.get(0).unwrap();
            let y = *nodes.get(1).unwrap();
            res[x][y] = true;
        }
    }

    res
}

pub fn count_edges(mat: &Vec<Vec<bool>>) -> usize {
    let mut res = 0;
    let xmax = mat.len();
    let ymax = mat.first().unwrap().len();
    for x in 0..xmax {
        for y in x..ymax {
            if mat[x][y] {
                res += 1;
            }
        }
    }
    res
}