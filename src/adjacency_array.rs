use std::collections::{HashMap, VecDeque};
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::path::Path;

pub fn make_adjacency_array(path: &str, size: usize) -> HashMap<usize, Vec<usize>> {
    let mut res: HashMap<usize, Vec<usize>> = HashMap::with_capacity(size);

    let input_path = Path::new(path);
    let f = File::open(input_path).unwrap();
    let reader = BufReader::new(f);
    let lines = reader.lines().map(|l| l.unwrap());
    for line in lines {
        // println!("reading : {}", line);
        if line.chars().next().unwrap() != '#' {
            let nodes = line
                .split('\t')
                .map(|n| n.parse::<usize>().unwrap())
                .collect::<Vec<usize>>();
            let x = nodes.get(0).unwrap();
            let y = nodes.get(1).unwrap();
            if let Some(vec) = res.get_mut(x) {
                vec.push(*y);
            } else {
                res.insert(*x, vec![*y]);
            }
            if let Some(vec) = res.get_mut(y) {
                vec.push(*x);
            } else {
                res.insert(*y, vec![*x]);
            }
        }
    }

    res
}

pub fn count_edges(array: &HashMap<usize, Vec<usize>>) -> usize {
    let mut res = 0;
    for adjacents in array.values() {
        res += adjacents.len();
    }
    res / 2
}

fn get_max_val_pair(map: &HashMap<usize, usize>) -> (usize, usize) {
    let (mut res, mut maxval) = map.iter().next().unwrap();
    for (key, val) in map {
        if val > maxval {
            res = key;
            maxval = val;
        }
    }
    (*res, *maxval)
}

/// Breadth First Search
/// Returns the farthest node from init_node and their distance
pub fn bfs(array: &HashMap<usize, Vec<usize>>, init_node: usize) -> (usize, usize) {
    if let None = array.get(&init_node) {
        panic!("Node not in graph!");
    }
    let mut fifo = VecDeque::new();
    fifo.push_back(init_node);
    let mut visited = HashMap::new();
    visited.insert(init_node, 0);
    while let Some(node) = fifo.pop_front() {
        let dist = **(&visited.get(&node).unwrap());
        for neigh in array.get(&node).unwrap() {
            if let None = &visited.get(neigh) {
                fifo.push_back(*neigh);
                visited.insert(*neigh, dist + 1);
            }
        }
    }
    get_max_val_pair(&visited)
}

/// Counts the number of triangles in a graph
pub fn triangles(array: &HashMap<usize, Vec<usize>>) -> usize {
    // preprocessing the map
    // let mut array = HashMap::with_capacity(old_array.len());
    // for key in old_array.keys() {
    //     let neighbors = old_array.get(key).unwrap();
    //     let mut new_neighbors = Vec::with_capacity(neighbors.len());
    //     for neigh in neighbors {
    //         if neigh > key {
    //             new_neighbors.push(*neigh);
    //         }
    //     }
    //     new_neighbors.sort();
    //     array.insert(*key, new_neighbors);
    // }

    let mut res = 0;
    // for each node of the graph
    for (node, neighbors) in array {
        // for each neighbor of the first node
        for neigh1 in neighbors {
            // we consider only triangles ABC such that A < B < C
            if node > neigh1 {
                continue;
            }
            let neighbors_of_1 = array.get(&neigh1).unwrap();
            // for each neighbor of the second node
            for neigh2 in neighbors_of_1 {
                if node > neigh2 || neigh1 > neigh2 {
                    continue;
                }
                // if it is also neighbor of the first, then we have a triangle
                if neighbors.contains(neigh2) {
                    res += 1;
                }
            }
        }
    }
    res
}
